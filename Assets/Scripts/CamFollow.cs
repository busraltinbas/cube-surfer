﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour
{
    public Transform Player;
    public GameObject Starter;
    public Vector3 offset;
    void Update()
    {

        transform.position = Player.transform.position + offset;
       
    }
}
