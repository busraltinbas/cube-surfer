﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    public FloatingJoystick floatingJoystick;
    public Transform Cubes;
    public Transform Starter;
    public Text ScoreTxt;
    public int score;
    public GameObject GameOverPnl;
    public Text DiamondTxt;
    public int DiamondNumber;
    public GameObject Particle_Effect;
  
    void Update()
    {
        Move();
       
    }

  public void AgainGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Scenes/Oyun");
    }

    void Move()
    {
        transform.position += Vector3.forward * 7 * Time.deltaTime;
        transform.position = new Vector3(floatingJoystick.Horizontal * 3, transform.position.y, transform.position.z);
    }


   
    void StarterUp()
    {

        Starter.transform.localPosition += new Vector3(0, 1, 0);

    }
    void StarterDown(float height)
    {
      
        Starter.transform.localPosition -= new Vector3(0, height, 0);
    }

    void CubesControl()
    {
        int index=0;
        foreach (Transform item in Cubes)
        {
         
            item.localPosition = new Vector3(0, index, 0);
            index++;
        }

    }

    void CubesRemove(float height)
    {
        
        for (int i = 0; i<height; i++)
        {

            Cubes.GetChild(0).parent = null;

             
        }

    }

  
  

    void ScorePlus()
    {
        score = score + 5;
        ScoreTxt.text = "SCORE:" + score;
    }

    IEnumerator f_Down(Collider col,float height)
    {
        col.gameObject.GetComponent<BoxCollider>().enabled = false;
        
        CubesRemove(height);
        yield return new WaitForSeconds(0.2f);
        StarterDown(height);
        CubesControl();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Complex")
        {
            col.transform.parent = Cubes;
            col.transform.localPosition = Vector3.zero;
            col.gameObject.GetComponent<BoxCollider>().enabled = false;
            StarterUp();
            CubesControl();
            ScorePlus();

        }

        
        if (col.gameObject.tag == "Barrier")

        {
            float height = col.gameObject.GetComponent<Barrier>().Height;
            if (Starter.transform.position.y <= height )
            {
                Time.timeScale = 0;
                GameOverPnl.SetActive(true);
            }
            if (Starter.transform.position.y > height )
            {
                StartCoroutine(f_Down(col,height));
                
            }           
        }

        if (col.gameObject.tag == "Diamond")
        {

            Destroy(col.gameObject);
            Instantiate(Particle_Effect, col.transform.position, Quaternion.identity);
            
        }

        if (col.gameObject.tag == "Diamond")
        {
            DiamondNumber++;
            DiamondTxt.text = "DIAMOND:" + DiamondNumber;
        }

     
    }
    
    
}
        
