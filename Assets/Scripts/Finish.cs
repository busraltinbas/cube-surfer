﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Finish : MonoBehaviour
{

    public GameObject YouWinPnl;
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            Time.timeScale = 0;
            YouWinPnl.SetActive(true);
        }
    }

   
}
